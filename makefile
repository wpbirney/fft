#simple template for a makefile

EXE=fft

CXX=g++
CXXFLAGS=-O3 -std=c++11 -Wall
LDFLAGS=-s

HEADERS=

OBJECTS=fft.o

all: $(EXE)

$(EXE): $(OBJECTS)
	$(CXX) $(OBJECTS) -o $(EXE) $(LDFLAGS)

%.o:%.cpp
	$(CXX) -c $(CXXFLAGS) $< -o $@

clean:
	rm -vf *.o $(EXE)
