#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define DEFAULT_PORT		42042
#define FRAGMENT_DATA_LEN	(1024*1024)
#define WRITE_BLOCK_SIZE	4096

using namespace std;

typedef unsigned long long u64;

struct fileInfo
{
	u64 blocks;
	u64 fileLen;
};

struct fileFragment
{
	u64 dataLen;
	char buffer[FRAGMENT_DATA_LEN];
};

string getPercentage(int a, int b)
{
	float x = a;
	float y = b;

	int percent = (x / y) * 100;
	stringstream ss;
	ss << percent << "%";
	return ss.str();
}

int transmit(string cmdline[])
{
	int sock = socket(AF_INET, SOCK_STREAM, 0);

	sockaddr_in target;
	target.sin_port = htons(DEFAULT_PORT);
	target.sin_addr.s_addr = inet_addr(cmdline[2].c_str());
	target.sin_family = AF_INET;

	int r = connect(sock, (sockaddr*)&target, sizeof(sockaddr_in));
	if(r == -1)
		return -1;

	ifstream file(cmdline[1].c_str(), ios::binary);
	file.seekg(0, ios::end);
	u64 len = file.tellg();
	file.seekg(0, ios::beg);

	u64 bytesLeft = len;
	u64 blks = len / FRAGMENT_DATA_LEN;

	cout << "total blocks to send: " << blks << endl
	     << "total bytes: " << bytesLeft << endl;

	fileInfo finfo;
	finfo.blocks = blks;
	finfo.fileLen = bytesLeft;

	//send the recv'r the fileInfo
	send(sock, (void*)&finfo, sizeof(fileInfo), 0);

	char resp = 0;
	recv(sock, (void*)&resp, sizeof(char), 0);

	for(u64 i = 0; i <= blks; ++i)	{
		fileFragment frag;
		if(bytesLeft >= FRAGMENT_DATA_LEN)	{
			frag.dataLen = FRAGMENT_DATA_LEN;
			file.read(frag.buffer, frag.dataLen);
		} else {
			frag.dataLen = bytesLeft;
			file.read(frag.buffer, frag.dataLen);
		}
		send(sock, (void*)&frag, sizeof(fileFragment), 0);
		bytesLeft -= frag.dataLen;
		cout << "> " << i << "/" << blks << " " << getPercentage(i, blks) << "\r";
		cout.flush();
	}

	file.close();
	return 0;
}

int recvFile(string cmdline[])
{
	int sock = socket(AF_INET, SOCK_STREAM, 0);

	sockaddr_in target;
	target.sin_port = htons(DEFAULT_PORT);
	target.sin_addr.s_addr = INADDR_ANY;
	target.sin_family = AF_INET;

	int r = bind(sock, (sockaddr*)&target, sizeof(sockaddr_in));
	if(r == -1)
		return -1;

	r = listen(sock, 5);
	if(r == -1)
		return -1;

	sockaddr_in clientAddr;
	socklen_t clientLen = sizeof(sockaddr_in);
	int client = accept(sock, (sockaddr*)&clientAddr, &clientLen);

	fileInfo finfo;
	recv(client, (void*)&finfo, sizeof(fileInfo), MSG_WAITALL);

	cout << "blocks: " << finfo.blocks << endl
	     << "bytes: " << finfo.fileLen << endl;

	string filename = cmdline[1];

	//create the output file and fill it to the recv'd file size
	ofstream out(filename.c_str(), ios::binary);

	//notify the sender we are ready for the data
	char resp = 0;
	send(client, (void*)&resp, sizeof(char), 0);

	for(u64 i = 0; i <= finfo.blocks; ++i)	{
		fileFragment frag;
		recv(client, (void*)&frag, sizeof(fileFragment), MSG_WAITALL);
		out.write(frag.buffer, frag.dataLen);
		cout << "> " << i << "/" << finfo.blocks << " " << getPercentage(i, finfo.blocks) << "\r";
		cout.flush();
	}

	out.close();
	return 0;
}

void usage()
{
	cerr << "invalid command line WTF!" << endl
	     << "usage: fft [recv/[send [file] [target ip]]" << endl
	     << "\tfft recv file.mkv" << endl
	     << "\t\trecv a file" << endl
	     << "\tfft send ~/file.mkv 192.168.1.14" << endl
	     << "\t\tto send a file" << endl;
}

int main(int argc, char** argv)
{
	string cmdline[argc-1];
	for(int i = 1; i < argc; ++i)
		cmdline[i-1] = argv[i];

	if(argc == 4)	{
		if(cmdline[0] == "send") {
			return transmit(cmdline);
		}
	} else if(argc == 3)	{
		if(cmdline[0] == "recv")
			return recvFile(cmdline);
	}

	usage();
	return 0;
}
